package battle;
import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;

import java.awt.*;
import javax.swing.*;
import java.io.*;
import javax.sound.sampled.*;

import chara.*;
import gui.*;

public class BattleHandler {
	private Main _main;
	public Role[] _alliance;
	public Role[] _enemy;
	public Role[] roleOrder;
	public int [] mapping;
	public int count;
	
	public BattleHandler(Main m, Role[] alliance, Role[] enemy) {
		_main = m;
		_alliance = alliance;
		_enemy = enemy;
		InitAndSort(alliance, enemy);
	}
	
	private void InitAndSort(Role[] alliance, Role[] enemy) {
		int i, j, k;
		
		int len = alliance.length + enemy.length;
		roleOrder = new Role[len];
		mapping = new int[len];
		
		Role tmp;
		for(i = 0; i < alliance.length; i++) {
			roleOrder[i] = alliance[i];
			mapping[i] = i + 1;
		}
		for(i = alliance.length; i < len; i++) {
			roleOrder[i] = enemy[i - alliance.length];
			mapping[i] = (-1) * (i - alliance.length + 1);
		}
		
		// sort by Spd.
		for(i = len - 1; i >= 0; i--) {
			for(j = 0; j < i; j++) {
				if(roleOrder[j].Spd > roleOrder[j + 1].Spd) {
					tmp = roleOrder[j];
					roleOrder[j] = roleOrder[j + 1];
					roleOrder[j + 1] = roleOrder[i];
					k = mapping[j];
					mapping[j] = mapping[j + 1];
					mapping[j + 1] = k;
				}
			}
		}
	}
	
	public int checkGameOver() {
		// If returns 1 : we lose
		// If returns -1 : we win.
		// If returns 0 : not gameover
		int gameover;
		boolean alli = true;
		boolean enem = true;
		int i;
		for(i = 0; i < _alliance.length; i++) {
			if(_alliance[i].nowHP > 0) {
				alli = false;
				break;
			}
		}
		for(i = 0; i < _enemy.length; i++) {
			if(_enemy[i].nowHP > 0) {
				enem = false;
				break;
			}
		}
		if(alli == true) {
			gameover = 1;
		}
		else if(enem == true) {
			gameover = -1;
		}
		else {
			gameover = 0;
		}
		return gameover;
	}
	
	public int getWhosTurn(int index) {
		return mapping[index];
	}
	
	public int accumulateRound(int who) {
		_alliance[who - 1].waitRound();
		return _alliance[who - 1].accuRound;
	}
	public void doAction(int who, int command, int enemy_num) {
		int _who = who - 1;
		switch(command) {
			case 0:
				_alliance[_who].normalAttack(_enemy[enemy_num]);
				PlaySoundThread p1 = new PlaySoundThread("./sound/effect/Punch_1.wav");
				p1.start();
				break;
			case 1:
				if(_alliance[_who] instanceof NormalAttacker) {
					NormalAttacker na = (NormalAttacker) _alliance[_who];
					na.PowerPunch(_enemy[enemy_num]);
					PlaySoundThread p2 = new PlaySoundThread("./sound/effect/Punch_2.wav");
					p2.start();
				}
				break;
			case 2:
				if(_alliance[_who] instanceof NormalAttacker) {
					NormalAttacker na = (NormalAttacker) _alliance[_who];
					na.RampageKick(_enemy[enemy_num]);
					PlaySoundThread p3 = new PlaySoundThread("./sound/effect/Slash6.wav");
					p3.start();
				}
				break;
			case 3:
				if(_alliance[_who] instanceof NormalAttacker) {
					NormalAttacker na = (NormalAttacker) _alliance[_who];
					na.AbsorbHit(_enemy[enemy_num]);
					PlaySoundThread p4 = new PlaySoundThread("./sound/effect/Slash3.wav");
					p4.start();
				}
				break;
			default:
					break;
		}
	}
	public void enemyAction(int enemy_who, int enemyCommand, int target) {
		if(enemyCommand == 0) {
			_enemy[enemy_who].normalAttack(_alliance[target]);
		}
	}
}


class PlaySoundThread extends Thread {
    String musicpath;
    PlaySoundThread(String music){ this.musicpath = music; }
    public void run() {
		try {
		    File sf = new File(musicpath);
		    AudioInputStream astr = AudioSystem.getAudioInputStream(sf);
		    AudioFormat afmt = astr.getFormat();
		    DataLine.Info inf = new DataLine.Info(SourceDataLine.class,afmt);
		    SourceDataLine l = (SourceDataLine) AudioSystem.getLine(inf);
		    l.open(afmt);
		    l.start();
		    byte[] buf = new byte[65536];
		    for( int n=0; (n=astr.read(buf,0,buf.length))>0; )
		    {
				l.write(buf,0,n);
		    }
		    l.drain();
		    l.close();
		} catch (Exception e) {
 		   e.printStackTrace();
		}
	}
}
