package gui;
import battle.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import chara.*;

public class BattleUI extends JPanel implements ActionListener, Runnable{
	private static final long serialVersionUID = 4333443258662366835L;
	private BattleHandler BH;
	private String battleName = "BattleName";

	private JSplitPane battleSplitPane;
	private JSplitPane msgControlSplitPane;
	private JPanel msgPanel;
	private JPanel ctrlPanel;
	private JPanel themePanel;
	
	private CardLayout ctrlCard;
	private JPanel[] cards;
	private JButton normalAttackButton;
	private JButton useSkillButton;
	private JButton accuRoundButton;
	private JButton skillButton;
	
	private JButton[] targetButton;
	
	private ImageIcon themeImage;
	private String filename = "./img/BattleTheme.jpg";
	private JLabel themeLabel;
	
	private ImageIcon[] allianceImage;
	private ImageIcon[] enemyImage;
	
	private JTextArea msgArea;
	
	private String[] msg;
	
	private Main main_object;
	private Role[] _alliance;
	private Role[] _enemy;
	private Mapper _mapper;
	
	private int Who;
	private int playerCommand;
	// 0 : normal attack
	// 1 : skill 1, 2 : skill 2, 3 : skill 3
	// 4 : accumulate round
	private boolean endButtonFlag;
	
	private int roundLength;
	
	private Container tmpContainer;
	
	public BattleUI(Main _main) {
		// Battle UI Constructor.
		
		main_object = _main;
		this.setLayout(null);
		
		// Initialize messages.
		msg = new String[10];
		int i;
		for(i = 0; i < 10; i++) {
			msg[i] = "";
		}
		
		// Draw basic battle theme.
		createBattleTheme(_main);

	}
	
	private void createBattleTheme(Main _main) {
		themePanel = new JPanel();
		msgPanel = new JPanel();
		ctrlPanel = new JPanel();
		
		tmpContainer = (Container) ctrlPanel;
		
		msgArea = new JTextArea(12, 45);
		msgArea.setText("Battle !");
		msgArea.setEnabled(false);
		msgArea.setDisabledTextColor(Color.BLACK);
		msgPanel.add(msgArea);

		msgControlSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, msgPanel, ctrlPanel);
		
		battleSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, themePanel, msgControlSplitPane);
		msgControlSplitPane.setDividerLocation(512);
		msgControlSplitPane.setOneTouchExpandable(false);
		msgControlSplitPane.setDividerSize(5);	
		battleSplitPane.setDividerLocation(512);
		battleSplitPane.setOneTouchExpandable(false);
		battleSplitPane.setDividerSize(5);
		
		battleSplitPane.setBounds(0, 0, 1024, 768);
		
		this.add(battleSplitPane);

	}
	
	public void startFight(Role[] alliance, Role[] enemy, Mapper ma) {
		_alliance = alliance;
		_enemy = enemy;
		_mapper = ma;
		
		// add a battle handler.
		BH = new BattleHandler(main_object, alliance, enemy);
		
		msgArea.setText("Battle !\n" + enemy.length + " Enemies !");
		
		setEnemies(enemy);
		setAlliances(alliance);
		setTheme();
		
		createCtrlCards(alliance.length, alliance, enemy);

		ctrlCard.show(tmpContainer, "card" + alliance.length);
		
		// roundLength = Total number of roles.
		roundLength = alliance.length + enemy.length;
		
		Thread th = new Thread(this);
		th.start();
	}
	
	public void _startFight(Role[] alliance, Role[] enemy) {
		_alliance = alliance;
		_enemy = enemy;
		
		// add a battle handler.
		BH = new BattleHandler(main_object, alliance, enemy);
		
		msgArea.setText("Battle !\n" + enemy.length + " Enemies !");
		
		setEnemies(enemy);
		setAlliances(alliance);
		setTheme();
		
		createCtrlCards(alliance.length, alliance, enemy);

		ctrlCard.show(tmpContainer, "card" + alliance.length);
		
		// roundLength = Total number of roles.
		roundLength = alliance.length + enemy.length;
		
		Thread th = new Thread(this);
		th.start();
	}
	
	public void run() {
		// go into standard battle flow.
		battleFlow(roundLength);
	}
	
	private void setTheme() {
		// Get theme Image.
		themePanel.setLayout(null);
		themeImage = getImage(filename);
		themeLabel = new JLabel(themeImage);
		themeLabel.setBounds(0, 0, 1024, 512);
		themePanel.add(themeLabel);
	}
	
	private void setAlliances(Role[] alliance) {
		allianceImage = new ImageIcon[alliance.length];
		int i;
		JLabel tmp;
		for(i = 0; i < alliance.length; i++) {
			allianceImage[i] = getImage(alliance[i].imagePath);
			tmp = new JLabel(allianceImage[i]);
			tmp.setBounds(200 + (i * 20), 180 - (i * 50), alliance[i].imageWidth, alliance[i].imageHeigth);
			themePanel.add(tmp);    
		}
	}
	
	private void setEnemies(Role[] enemy) {
		enemyImage = new ImageIcon[enemy.length];
		int i;
		JLabel tmp;
		System.err.printf("%d\n", enemy.length);
		for(i = 0; i < enemy.length; i++) {
			enemyImage[i] = getImage(enemy[i].imagePath);
			tmp = new JLabel(enemyImage[i]);
			tmp.setBounds(650 - (i * 30), 320 - (i * 30), enemy[i].imageWidth, enemy[i].imageHeigth);
			themePanel.add(tmp);
		}
	}
	
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void createCtrlCards(int len, Role[] alliance, Role[] enemy) {
		
		ctrlCard = new CardLayout();
		ctrlPanel.setLayout(ctrlCard);
		cards = new JPanel[len + 3];
		int i;
		for(i = 0; i < len + 3; i++) {
			cards[i] = new JPanel();
		}
		
		setFirstActionButton(len);
		setTargetChoiceButton(len, enemy.length);
		
		for(i = 0; i < len; i++) {
			cards[i].setLayout(new FlowLayout());
			setRoleSkillButton(i, alliance[i]);
		}
		
		for(i = 0; i < len + 3; i++) {
			ctrlPanel.add(cards[i], "card" + i);
		}
		
		ctrlCard.show(tmpContainer, "card" + len);
	}
	
	private void setRoleSkillButton(int num, Role me) {
		int i;
		for(i = 0; i < 3; i++) {
			skillButton = new JButton(me.skillString[i]);
			skillButton.setActionCommand("Skill" + i);
			skillButton.addActionListener(this);
			cards[num].add(skillButton);
		}
	}
	
	private void setTargetChoiceButton(int len, int elen) {
		targetButton = new JButton[elen];
		
		cards[len + 1].setLayout(new FlowLayout());
		
		int i;
		for(i = 0; i < elen; i++) {
			targetButton[i] = new JButton("Enemy " + i);
			targetButton[i].setActionCommand("" + i);
			targetButton[i].addActionListener(this);
			cards[len + 1].add(targetButton[i]);
		}
	}
	
	private void setFirstActionButton(int len) {
		cards[len + 2].setLayout(new FlowLayout());
		
		normalAttackButton = new JButton("Normal Attack");
		useSkillButton = new JButton("Use Skill");
		accuRoundButton = new JButton("Accumulate a Round");
		normalAttackButton.setActionCommand("NormalAttack");
		useSkillButton.setActionCommand("UseSkill");
		accuRoundButton.setActionCommand("AccuRound");
		normalAttackButton.addActionListener(this);
		useSkillButton.addActionListener(this);
		accuRoundButton.addActionListener(this);
		
		cards[len + 2].add(normalAttackButton);
		cards[len + 2].add(useSkillButton);
		cards[len + 2].add(accuRoundButton);
	}

	private void battleFlow(int roundLength) {
		int i;
		int gameover;
		boolean endBattleFlag = false;

		int whosturn;

		while(!endBattleFlag) {
			
			// for each round, we have "roundLength" roles to act.
			for(i = 0; i < roundLength; i++) {
				
				// before each role acts, we check whether the game is over or not.
				gameover = BH.checkGameOver();
				endBattleFlag = gameoverHandler(gameover);
				if(endBattleFlag == true) {
					break;
				}

				// ask battle handler whose turn next in this round.
				whosturn = BH.getWhosTurn(i);
				Who = whosturn;
				
				if(whosturn > 0) {
					// Its our turn.
					// Have to ask user what to do.
					ourTurn();
				}
				else {
					// Its enemy's turn.
					enemyTurn(whosturn);
				}
				
			}
		}
		
		ctrlPanel = new JPanel();
		tmpContainer = (Container) ctrlPanel;
		msgControlSplitPane.setRightComponent(ctrlPanel);
		
		main_object.card.show(main_object.getContentPane(), Main.mapperName);
		main_object.mapper.requestFocus();
		main_object.play.stop();
		main_object.play = new PlaySoundThread("./sound/bgm/map.wav");
		main_object.play.start();
	}
	
	private boolean gameoverHandler(int gameover) {
		if(gameover > 0) {
			// we win.
			int i;
			int sum = 0;
			for(i= 0; i < _enemy.length; i++) {
				sum += _enemy[i].accuExp;
			}
			sum /= _alliance.length;
			/*
			 * Still have to show some information to the user.
			 */
			return true;
		}
		else if(gameover < 0) {
			// we lose.
			
			/*
			 * Still have to show some information to the user.
			 */
			
			main_object.card.show(main_object.getContentPane(), Main.end1Name);
			main_object.menu.requestFocus();
			main_object.play.stop();
			main_object.play = new PlaySoundThread("./sound/effect/gameover_sound.wav");
			main_object.play.start();

			return true;
		}
		else {
			return false;
		}
	}	
	
	private void ourTurn() {
		int ttt = Who;
		msgArea.setText("Player\nHP: " + _alliance[ttt - 1].nowHP + " / " + _alliance[ttt - 1].maxHP + "\nMP: " + _alliance[ttt - 1].nowMP + " / " + _alliance[ttt - 1].maxMP + "\n\nEnemy\nHP: " + _enemy[0].nowHP + " / " + _enemy[0].maxHP);
		int index = _enemy.length + 2;
		ctrlCard.show(tmpContainer, "card" + index);
		endButtonFlag = false;
		while(!endButtonFlag) {
		}
	}
	
	private void enemyTurn(int whosturn) {
		Random ran = new Random();
		int enemy_who = ((-1) * whosturn) - 1;
		int enemyCommand = ran.nextInt(_enemy[enemy_who].SkillNum + 1);
		int target = ran.nextInt(_alliance.length);
		
		BH.enemyAction(enemy_who, enemyCommand, target);
	}
	
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		int num;
		int index;
		if(str.equals("NormalAttack")) {
			playerCommand = 0;
			index = _enemy.length + 1;
			ctrlCard.show(tmpContainer, "card" + index);
		}
		else if(str.equals("UseSkill")) {
			index = Who - 1;
			ctrlCard.show(tmpContainer, "card" + index);
		}
		else if(str.equals("AccuRound")) {
			int rrr;
			rrr = BH.accumulateRound(Who);
			msgArea.setText("Not acts this round.\nYou have accumulated " + rrr + "rounds.");
			endButtonFlag = true;
		}
		else {
			try {
				num = Integer.parseInt(str);
				// Target part.
				BH.doAction(Who, playerCommand, num);
				endButtonFlag = true;
			} catch(NumberFormatException ne) {
				// Skill part.
				if(str.equals("Skill0")) {
					playerCommand = 1;
				}
				else if(str.equals("Skill1")) {
					playerCommand = 2;
				}
				else if(str.equals("Skill2")) {
					playerCommand = 3;
				}
				index = _enemy.length + 1;
				ctrlCard.show(tmpContainer, "card" + index);
			}
		}
	}
}
