package gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MainMenu extends JPanel implements ActionListener {
	/*
	 * make eclipse happy
	 */
	private static final long serialVersionUID = -6270469403286500943L;
	/*
	 * main function pointer
	 */
	private Main main_object;
	/*
	 * Background setting
	 */
	// Background image file name
	private final String filename = "./img/MainMenuBackground.jpg";
	// Background Image
	private ImageIcon backImage;
	// Background Label
	private JLabel BackgroundLabel;
	/*
	 * Title setting
	 */
	// Title name
	private final String Title = "Final Fantasy Good Game";
	// Title label
	private JLabel TitleLabel;
	/*
	 * main menu buttons
	 */
	private final String StartAction = "start";
	private JButton StartButton;
	private final String LoadAction = "load";
	private JButton LoadButton;
	/*
	 * constructor
	 */
	public MainMenu(Main _main) {
		// set main
		main_object = _main;
		// panel setting
		this.setLayout(null);
		// Get the image.
		backImage = getImage(filename);
		// Create and Initialize the Main Menu.
		createMenu();
	}
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/*
	 * 
	 */
	private void createMenu() {
		/*
		 * Create and initialize title label.
		 */
		TitleLabel = new JLabel(Title);
		TitleLabel.setFont(new Font("monospaced", Font.BOLD, 48));
		TitleLabel.setForeground(Color.yellow);
		/*
		 * Set Background image label.
		 */
		BackgroundLabel = new JLabel(backImage);
		/*
		 * Set the Buttons.
		 */
		StartButton = new JButton("New Game");
		LoadButton = new JButton("Load Game");
		StartButton.setActionCommand(StartAction);
		LoadButton.setActionCommand(LoadAction);
		StartButton.addActionListener(this);
		LoadButton.addActionListener(this);
		/*
		 * Set the locations of all GUI items.
		 */
		TitleLabel.setBounds(230, 30, 800, 160);
		StartButton.setBounds(300, 560, 180, 45);
		LoadButton.setBounds(550, 560, 180, 45);
		BackgroundLabel.setBounds(0, 0, 1024, 768);
		/*
		 * add component into main menu
		 */	
		
		this.add(TitleLabel);
		this.add(StartButton);
		this.add(LoadButton);
		this.add(BackgroundLabel);
		
	}
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if (str.equals(StartAction)) {
			System.err.println("New a RPG ... ");
			Main.rpg = new RPG();
			main_object.mapper.setNewMap();
			main_object.card.show(main_object.getContentPane(), Main.stName);
			main_object.mapper.requestFocus();
			main_object.play.stop();
			main_object.play = new PlaySoundThread("./sound/bgm/story.wav");
			main_object.play.start();
		}
		else if (str.equals(LoadAction)) {
			main_object.card.show(main_object.getContentPane(), Main.loaderName);
		}
	}
}
