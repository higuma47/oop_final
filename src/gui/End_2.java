package gui;

import javax.swing.*;

public class End_2 extends JPanel {
	private ImageIcon backImage;
	private String backPath = "./img/End_2.jpg";
	private JLabel backLabel;
	
	public End_2() {
		this.setLayout(null);
		backImage = getImage(backPath);
		createTheme();
	}
	
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void createTheme() {
		
		backLabel = new JLabel(backImage);

		backLabel.setBounds(0, 0, 1024, 768);
		
		this.add(backLabel);
	}
	
}
