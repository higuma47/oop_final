package gui;

import java.awt.*;
import javax.swing.*;
//import java.awt.event.*;
//import javax.swing.*;
//import javax.swing.text.*;
//import java.util.concurrent.*;
import java.io.*;
import javax.sound.sampled.*;

public class Main extends JFrame {
	/*
	 * make eclipse happy
	 */
	private static final long serialVersionUID = 2260698574724227008L;
	/*
	 * RPG Object and JPanels
	 */
	public static RPG rpg;
	public MainMenu menu;
	public Recorder saver, loader;
	public BattleUI battle_ui;
	public Mapper mapper;
	public StartTheme st;
	public ExitMenu em;
	public endTheme_onClass end;
	public End_1 end1;
	public End_2 end2; 
	public CardLayout card;
	PlaySoundThread play = new PlaySoundThread("./sound/bgm/opening.wav");
	/*
	 * JPanel names
	 */
	public static final String rpgName = "RPG";
	public static final String menuName = "Menu";
	public static final String saverName = "Saver";
	public static final String loaderName = "Loader";
	public static final String battleUIName = "BattleUI";
	public static final String mapperName = "Mapper";
	public static final String emName = "ExitMenu";
	public static final String stName = "StartTheme";
	public static final String endName = "EndName";
	public static final String end1Name = "End1Name";
	public static final String end2Name = "End2Name";
	/*
	 * JFrame setting
	 */
	// Title string.
	private final String Title = "OOP Final Project";
	// Frame : 1024 * 768
	public final static int window_width = 1024;
	public final static int window_height = 768;
	
	public boolean load_game_or_not;
	
	/*
	 * constructor
	 */
	public Main() {
		System.err.println("Load rpg ...");
		rpg = null;
		System.err.println("Load menu ...");
		menu = new MainMenu(this);
		System.err.println("Load saver ...");
		saver = new Recorder(saverName, this);
		System.err.println("Load loader ...");
		loader = new Recorder(loaderName, this);
		System.err.println("Load battle UI ...");
		battle_ui = new BattleUI(this);
		System.err.println("Load mapper ...");
		mapper = new Mapper(this);
		
		em = new ExitMenu(this);
		st = new StartTheme(this);
		end = new endTheme_onClass(this);
		end1 = new End_1();
		end2 = new End_2();
		/*
		 * set JFrame
		 */
		System.err.println("Set frame ...");
		this.card = new CardLayout();
		this.setLayout(card);
		// Create the Frame and set its size 1024 * 768
		this.setTitle(Title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(window_width, window_height);
		this.setResizable(false);
		this.setVisible(true);
		// add every component
		System.err.println("Add frame components ...");
		this.add(menu, menuName);
		this.add(saver, saverName);
		this.add(loader, loaderName);
		this.add(battle_ui, battleUIName);
		this.add(mapper, mapperName);
		this.add(st, stName);
		this.add(end, endName);
		this.add(end1, end1Name);
		this.add(end2, end2Name);
		
		System.err.println("Start menu ...");
		this.card.show(this.getContentPane(), menuName);

		play.start();
	}
	public static void main(String[] args) {
		new Main();
		return ;
	}
}

class PlaySoundThread extends Thread {
    String musicpath;
    PlaySoundThread(String music){ this.musicpath = music; }
    public void run() {
		try {
		    File sf = new File(musicpath);
		    AudioInputStream astr = AudioSystem.getAudioInputStream(sf);
		    AudioFormat afmt = astr.getFormat();
		    DataLine.Info inf = new DataLine.Info(SourceDataLine.class,afmt);
		    SourceDataLine l = (SourceDataLine) AudioSystem.getLine(inf);
		    l.open(afmt);
		    l.start();
		    byte[] buf = new byte[65536];
		    for( int n=0; (n=astr.read(buf,0,buf.length))>0; )
		    {
				l.write(buf,0,n);
		    }
		    l.drain();
		    l.close();
		} catch (Exception e) {
 		   e.printStackTrace();
		}
	}
}
