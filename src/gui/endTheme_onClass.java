package gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

import chara.*;

public class endTheme_onClass extends JPanel implements ActionListener{
	private static final long serialVersionUID = 2415220571377312631L;
	
	private Main _main;
	
	private JTextArea textArea;
	
	private ImageIcon backImage;
	private String backPath = "./img/EndThemeInClass.png";
	
	private JLabel backLabel;

	private String[] str;
	private String strPath = "./text/LastTalk.txt";
	private String areaString;
	
	private JButton Answer;
	private JButton Pass;
	
	public endTheme_onClass(Main m) {
		_main = m;
		areaString = "";
		this.setLayout(null);
		backImage = getImage(backPath);
		createTheme();
	}
	
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void createTheme() {
		textArea = new JTextArea(10, 45);
		textArea.setFont(new Font("monospaced", Font.BOLD, 16));
		textArea.setForeground(Color.BLACK);
		
		str = new String[30];
		try {
			String line;
			BufferedReader reader = new BufferedReader(new FileReader(strPath));
			int cc = 0;
			while ( (line = reader.readLine()) != null ) {
				str[cc] = line;
				cc++;
			}
		} catch(Exception e) {
			System.out.println ( "Error:" + e.getMessage() );
		}
		
		int kkk;
		for(kkk = 0; kkk < 10; kkk++) {
			if(str[kkk] != null) {
				areaString = areaString + str[kkk] + "\n";
			}
		}
		textArea.setText(areaString);
		
		
		textArea.setEnabled(false);
		//textArea.setOpaque(false);
		
		backLabel = new JLabel(backImage);
		
		Answer = new JButton("Answer");
		Answer.setActionCommand("Answer");
		Answer.addActionListener(this);
		
		Pass = new JButton("Pass");
		Pass.setActionCommand("Pass");
		Pass.addActionListener(this);
		
		Answer.setBounds(210, 600, 180, 45);
		Pass.setBounds(630, 600, 180, 45);
		textArea.setBounds(310, 220, 390, 247);
		backLabel.setBounds(0, 0, 1024, 768);
		
		this.add(Answer);
		this.add(Pass);
		this.add(textArea);
		this.add(backLabel);
	}

	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand();
		if(s.equals("Answer")) {
			_main.card.show(_main.getContentPane(), Main.end1Name);
			_main.play.stop();
			_main.play = new PlaySoundThread("./sound/effect/gameover_2.wav");
			_main.play.start();
		}
		else if(s.equals("Pass")) {
			_main.card.show(_main.getContentPane(), Main.end2Name);
			_main.play.stop();
			_main.play = new PlaySoundThread("./sound/effect/gameover_sound.wav");
			_main.play.start();
		}
	}
	
	public void callBattleUI() {
		Role[] enemy = new Role[1];
		enemy[0] = new Swordman();
		_main.card.show(_main.getContentPane(), Main.battleUIName);
		_main.play.stop();
		_main.play = new PlaySoundThread("./sound/bgm/battle.wav");
		_main.play.start();
		_main.battle_ui._startFight(Main.rpg.toArray(), enemy);
	}
}
