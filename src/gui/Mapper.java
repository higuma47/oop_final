package gui;

import chara.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

public class Mapper extends JPanel implements KeyListener {
	private static final long serialVersionUID = 5312491571737406911L;
	
	private static final int M_UP = 0;
	private static final int M_LEFT = 1;
	private static final int M_DOWN = 2;
	private static final int M_RIGHT = 3;
	
	private Main main_object;
	
	private String mapPath = "./img/map/map001.png";
	private ImageIcon mapImage;
	private JLabel mapLabel;
	
	private String charPath = "./img/icon/down_2.png";
	private ImageIcon charImage;
	private JLabel charLabel;
	
	public int slime_num = 5;
	String sss = "Remain Slime : ";
	private String msg;
	private JLabel msgLabel;
	
	private int[][] Coo;
	public int currentX, currentY;
	public int[] slimeX, slimeY;
	
	private String tmp;
	private int coox, cooy;
	private int direction;
	private int d_count;

	
	public Mapper(Main _main) {
		main_object = _main;
		
		mapImage = getImage(mapPath);
		mapLabel = new JLabel(mapImage);
		
		charImage = getImage(charPath);
		charLabel = new JLabel(charImage);
		d_count = 2;
		charLabel.setBounds(32 * 16, 32 * 12, 32, 32);
		
		msg = sss + slime_num;
		msgLabel = new JLabel(msg);
		msgLabel.setFont(new Font("monospaced", Font.BOLD, 48));
		msgLabel.setBounds(0,0,400,50);
		
		this.add(msgLabel);
		this.add(charLabel);
		this.add(mapLabel);
		
		this.setLayout(null);
		this.addKeyListener(this);
		
		Coo = null;
		slimeX = new int[5];
		slimeY = new int[5];
	}
	
	private void loadCoo(String filename)
	{
		Coo = new int[128][128];
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for (int y = 0; y < 128; y++)
			for (int x = 0; x < 128; x++)
				Coo[x][y] = scanner.nextInt();
		int num = scanner.nextInt();
		for (int i = 0; i < num; i++)
		{
			slimeX[i] = scanner.nextInt();
			slimeY[i] = scanner.nextInt();
			if (Main.rpg.isSlime[i]) Coo[slimeX[i]][slimeY[i]] = 2;
			else Coo[slimeX[i]][slimeY[i]] = 0;
		}
	}
	
	public void setMap() {
		currentX = 32 * (16 - Main.rpg.x);
		currentY = 32 * (12 - Main.rpg.y);
		mapLabel.setBounds(currentX, currentY, mapImage.getIconWidth(), mapImage.getIconHeight());
	}
	
	public void setNewMap() {
		loadCoo("./map/coo.out");
		direction = Main.rpg.face;
		slime_num = Main.rpg.slimeCount;
		msgLabel.setText(sss + slime_num);
		setMap();
	}
	
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void keyPressed(KeyEvent e) {
		if(slime_num == 0) {
			main_object.card.show(main_object.getContentPane(), Main.endName);
			main_object.end.requestFocus();
			main_object.play.stop();
			main_object.play = new PlaySoundThread("./sound/bgm/opening.wav");
			main_object.play.start();
		}
		coox = (-1) * currentX / 32 + 16;
		cooy = (-1) * currentY / 32 + 12;
		switch (e.getKeyCode()) {
			case KeyEvent.VK_DOWN:
				direction = M_DOWN;
				d_count++;
				if(d_count > 3) {
					d_count = 1;
				}
				tmp = "./img/icon/down_";
				charPath = tmp + d_count + ".png";
				charImage = getImage(charPath);
				charLabel.setIcon(charImage);
				if(Coo[coox][cooy + 1] == 1) {
					mapLabel.setBounds(currentX, currentY - 32, 4096, 4096);
					currentY -= 32;
					cooy++;
				}
				break;
			case KeyEvent.VK_LEFT:
				direction = M_LEFT;
				d_count++;
				if(d_count > 3) {
					d_count = 1;
				}
				tmp = "./img/icon/left_";
				charPath = tmp + d_count + ".png";
				charImage = getImage(charPath);
				charLabel.setIcon(charImage);
				if(Coo[coox - 1][cooy] == 1) {
					mapLabel.setBounds(currentX + 32, currentY, 4096, 4096);
					currentX += 32;
					coox--;
				}
				break ;
			case KeyEvent.VK_RIGHT:
				direction = M_RIGHT;
				d_count++;
				if(d_count > 3) {
					d_count = 1;
				}
				tmp = "./img/icon/right_";
				charPath = tmp + d_count + ".png";
				charImage = getImage(charPath);
				charLabel.setIcon(charImage);
				if(Coo[coox + 1][cooy] == 1) {
					mapLabel.setBounds(currentX - 32, currentY, 4096, 4096);
					currentX -= 32;
					coox++;
				}
				break ;
			case KeyEvent.VK_UP:
				direction = M_UP;
				d_count++;
				if(d_count > 3) {
					d_count = 1;
				}
				tmp = "./img/icon/up_";
				charPath = tmp + d_count + ".png";
				charImage = getImage(charPath);
				charLabel.setIcon(charImage);
				if(Coo[coox][cooy - 1] == 1) {
					mapLabel.setBounds(currentX, currentY + 32, 4096, 4096);
					currentY += 32;
					cooy--;
				}
				break ;
			case KeyEvent.VK_SPACE:
				main_object.em.callExitMenu(this);
				break;
			default:
				break ;
		}
		if(Coo[coox][cooy + 1] == 2) {
			callBattleUI();
			slime_num--;
			Main.rpg.slimeCount--;
			if(coox == 33 && cooy + 1 == 96) {
				Main.rpg.isSlime[0] = false;
			}
			else if(coox == 60 && cooy + 1 == 69) {
				Main.rpg.isSlime[1] = false;
			}
			else if(coox == 75 && cooy + 1 == 78) {
				Main.rpg.isSlime[2] = false;
			}
			else if(coox == 99 && cooy + 1 == 70) {
				Main.rpg.isSlime[3] = false;
			}
			else if(coox == 102 && cooy + 1 == 102) {
				Main.rpg.isSlime[4] = false;
			}
			msg = sss + slime_num;
			msgLabel.setText(msg);
			Coo[coox][cooy + 1] = 0;
		}
		else if(Coo[coox][cooy - 1] == 2) {
			callBattleUI();
			slime_num--;
			Main.rpg.slimeCount--;
			if(coox == 33 && cooy - 1 == 96) {
				Main.rpg.isSlime[0] = false;
			}
			else if(coox == 60 && cooy - 1 == 69) {
				Main.rpg.isSlime[1] = false;
			}
			else if(coox == 75 && cooy - 1 == 78) {
				Main.rpg.isSlime[2] = false;
			}
			else if(coox == 99 && cooy - 1 == 70) {
				Main.rpg.isSlime[3] = false;
			}
			else if(coox == 102 && cooy - 1 == 102) {
				Main.rpg.isSlime[4] = false;
			}
			msg = sss + slime_num;
			msgLabel.setText(msg);
			Coo[coox][cooy - 1] = 0;
		}
		else if(Coo[coox + 1][cooy] == 2) {
			callBattleUI();
			slime_num--;
			Main.rpg.slimeCount--;
			if(coox + 1 == 33 && cooy == 96) {
				Main.rpg.isSlime[0] = false;
			}
			else if(coox + 1 == 60 && cooy == 69) {
				Main.rpg.isSlime[1] = false;
			}
			else if(coox + 1 == 75 && cooy == 78) {
				Main.rpg.isSlime[2] = false;
			}
			else if(coox + 1 == 99 && cooy == 70) {
				Main.rpg.isSlime[3] = false;
			}
			else if(coox + 1 == 102 && cooy == 102) {
				Main.rpg.isSlime[4] = false;
			}
			msg = sss + slime_num;
			msgLabel.setText(msg);
			Coo[coox + 1][cooy] = 0;
		}
		else if(Coo[coox - 1][cooy] == 2) {
			callBattleUI();
			slime_num--;
			Main.rpg.slimeCount--;
			if(coox - 1 == 33 && cooy == 96) {
				Main.rpg.isSlime[0] = false;
			}
			else if(coox - 1 == 60 && cooy == 69) {
				Main.rpg.isSlime[1] = false;
			}
			else if(coox - 1 == 75 && cooy == 78) {
				Main.rpg.isSlime[2] = false;
			}
			else if(coox - 1 == 99 && cooy == 70) {
				Main.rpg.isSlime[3] = false;
			}
			else if(coox - 1 == 102 && cooy == 102) {
				Main.rpg.isSlime[4] = false;
			}
			msg = sss + slime_num;
			msgLabel.setText(msg);
			Coo[coox - 1][cooy] = 0;
		}
		System.err.printf("%d %d\n", coox, cooy);
		Main.rpg.x = coox;
		Main.rpg.y = cooy;
		Main.rpg.face = direction;
	}
	public void keyTyped(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
	
	public void callBattleUI() {
		Role[] enemy = new Role[1];
		enemy[0] = new Slime();
		main_object.card.show(main_object.getContentPane(), Main.battleUIName);
		main_object.play.stop();
		main_object.play = new PlaySoundThread("./sound/bgm/battle.wav");
		main_object.play.start();
		main_object.battle_ui.startFight(Main.rpg.toArray(), enemy, this);
	}
}
