package gui;

import java.io.*;
import java.awt.*;
import javax.swing.*;

public class RecordField extends JPanel {
	private static final long serialVersionUID = -7240351804342920617L;
	
	private static final String sep = File.separator;
	private String filename;
	private String path;
	
	private int num;
	private RPG rpg;
	private JLabel info;
	private JButton btn;
	private File file;
	
	public RecordField(int _num, JButton _btn) {
		num = _num;
		this.setLayout(new FlowLayout());
		
		filename = "" + num + ".sav";
		path = "." + sep + "save";
		file = new File(path, filename);
		
		readData();
		info = new JLabel(setInfo());
		btn = _btn;
		
		this.add(info);
		this.add(btn);
	}
	
	private void readData() {
		if (file.exists()) {
			try {
				FileInputStream fileIS = new FileInputStream(file);
				ObjectInputStream objectIS = new ObjectInputStream(fileIS); 
				rpg = (RPG)objectIS.readObject();
				objectIS.close();
			}
			catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
		else {
			file.getParentFile().mkdirs();
			rpg = null;
		}
		return ;
	}
	private String setInfo() {
		if (rpg != null)
			return "Name: " + filename + " x: " + rpg.x + " y: " + rpg.y + "\n";
		return "No Data";
	}
	public void refresh() {
		file = new File(path, filename);
		readData();
		info.setText(setInfo());
	}
	public RPG getRPG() {
		return rpg;
	}
	
	public boolean setRPG(RPG _rpg) {
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			FileOutputStream fileOS = new FileOutputStream(file);
			ObjectOutputStream objectOS = new ObjectOutputStream(fileOS);
			rpg = _rpg;
			objectOS.writeObject(rpg);
			objectOS.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
}
