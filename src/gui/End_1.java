package gui;

import javax.swing.*;

public class End_1 extends JPanel{
	private static final long serialVersionUID = 922869835936111402L;
	private ImageIcon backImage;
	private String backPath = "./img/End_1.jpg";
	private JLabel backLabel;
	
	public End_1() {
		this.setLayout(null);
		backImage = getImage(backPath);
		createTheme();
	}
	
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void createTheme() {
		
		backLabel = new JLabel(backImage);

		backLabel.setBounds(0, 0, 1024, 768);
		
		this.add(backLabel);
	}
	
}
