package gui;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Recorder extends JPanel implements ActionListener {
	/*
	 * make eclipse happy
	 */
	private static final long serialVersionUID = 8500981718695787155L;
	/*
	 * main function pointer
	 */
	private Main main_object;
	/*
	 * 
	 */
	private RecordField[] records;
	private JButton[] buttons;
	private static final int FILE_NUM = 3;
	private String name;
	
	public Recorder(String _name, Main _main) {
		main_object = _main;
		name = _name;
		records = new RecordField[FILE_NUM];
		buttons = new JButton[FILE_NUM];
		for (int i = 0; i < FILE_NUM; i++)
		{
			// saver
			if (name.equals(Main.saverName)) buttons[i] = new JButton("Save");
			// loader
			else buttons[i] = new JButton("Load");
			buttons[i].setActionCommand(name + " " + i);
			buttons[i].addActionListener(this);
			records[i] = new RecordField(i, buttons[i]);
		}
		
		this.setLayout(new GridLayout(FILE_NUM, 1));
		for (int i = 0; i < FILE_NUM; i++)
			this.add(records[i]);
	}
	
	public void refresh()
	{
		for (int i = 0; i < FILE_NUM; i++)
			records[i].refresh();
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		Scanner scanner = new Scanner(cmd);
		String str = scanner.next();
		int i = scanner.nextInt();
		scanner.close();
		if (str.equals(Main.saverName))
			records[i].setRPG(Main.rpg);
		else if (str.equals(Main.loaderName)) {
			Main.rpg = records[i].getRPG();
			main_object.mapper.setNewMap();
			main_object.play.stop();
			main_object.play = new PlaySoundThread("./sound/bgm/map.wav");
			main_object.play.start();
		}
		// call map
		main_object.loader.refresh();
		main_object.saver.refresh();
		main_object.card.show(main_object.getContentPane(), Main.mapperName);
		main_object.mapper.requestFocus();
	}
}
