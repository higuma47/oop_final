package gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class ExitMenu {
	private JMenuItem saveBtn;
	private JMenuItem loadBtn;
	private JMenuItem exitBtn;
	private JPopupMenu poMenu;
	
	private Main main_object;
	
	public ExitMenu(Main _main)
	{
		main_object = _main;
		saveBtn = new JMenuItem("save");
		saveBtn.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						main_object.card.show(main_object.getContentPane(), Main.saverName);
					}
				});
		loadBtn = new JMenuItem("load");
		loadBtn.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						main_object.card.show(main_object.getContentPane(), Main.loaderName);
					}
				});
		exitBtn = new JMenuItem("exit");
		exitBtn.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						main_object.play.stop();
						main_object.play = new PlaySoundThread("./sound/bgm/opening.wav");
						main_object.play.start();
						main_object.card.show(main_object.getContentPane(), Main.menuName);
					}
				});
		poMenu = new JPopupMenu();
		poMenu.add(saveBtn);
		poMenu.add(loadBtn);
		poMenu.add(exitBtn);
		poMenu.setPopupSize(150, 200);
	}
	public void callExitMenu(Container container)
	{
		poMenu.show(container, 437, 284);
	}
}
