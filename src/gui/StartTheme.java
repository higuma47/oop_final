package gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class StartTheme extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = -279039906925023694L;

	private Main _main;
	
	private JTextArea textArea;
	
	private ImageIcon backImage;
	private String backPath = "./img/StartTheme.jpg";
	
	private JLabel backLabel;

	private int count;
	private int max_count;
	private String[] str;
	private String strPath = "./text/FirstTalk.txt";
	private String areaString;
	
	private JButton next;
	
	
	public StartTheme(Main m) {
		_main = m;
		count = 0;
		areaString = "";
		this.setLayout(null);
		backImage = getImage(backPath);
		createTheme();
	}
	
	/*
	 * Return an image via file path.
	 */
	private ImageIcon getImage(String path) {
		ImageIcon image;
		try {
			image = new ImageIcon(path);
			return image;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void createTheme() {
		textArea = new JTextArea(10, 45);
		textArea.setFont(new Font("monospaced", Font.BOLD, 16));
		textArea.setForeground(Color.BLACK);
		
		str = new String[30];
		try {
			String line;
			BufferedReader reader = new BufferedReader(new FileReader(strPath));
			int cc = 0;
			while ( (line = reader.readLine()) != null ) {
				str[cc] = line;
				cc++;
			}
		} catch(Exception e) {
			System.out.println ( "Error:" + e.getMessage() );
		}
		max_count = (str.length / 10) + 1;
		
		int kkk;
		for(kkk = (count*10); kkk < (10*(count+1)); kkk++) {
			if(str[kkk] != null) {
				areaString = areaString + str[kkk] + "\n";
			}
		}
		count++;
		textArea.setText(areaString);
		
		
		textArea.setEnabled(false);
		//textArea.setOpaque(false);
		
		backLabel = new JLabel(backImage);
		
		next = new JButton("next");
		next.setActionCommand("Next");
		next.addActionListener(this);
		
		next.setBounds(420, 600, 180, 45);
		textArea.setBounds(320, 220, 390, 247);
		backLabel.setBounds(0, 0, 1024, 768);
		
		this.add(next);
		this.add(textArea);
		this.add(backLabel);
	}
	
	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand();
		if(s.equals("Next")) {
			if(max_count - count == 1) {
				_main.mapper.setMap();
				_main.card.show(_main.getContentPane(), Main.mapperName);
				_main.mapper.requestFocus();
				_main.play.stop();
				_main.play = new PlaySoundThread("./sound/bgm/map.wav");
				_main.play.start();
			}
			else {
				areaString = "";
				int kkk;
				for(kkk = (count*10); kkk < (10*(count+1)); kkk++) {
					if(str[kkk] != null) {
						areaString = areaString + str[kkk] + "\n";
					}
				}
				count++;
				textArea.setText(areaString);
			}
		}
	}
}
