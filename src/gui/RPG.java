package gui;

import chara.*;
import java.io.*;
import java.util.*;

public class RPG implements Serializable {
	/*
	 * make eclipse happy
	 */
	private static final long serialVersionUID = 4033024129168066830L;
	// Role array
	private ArrayList<Role> team;
	// map & position
	public int x, y, face;
	
	public boolean[] isSlime;
	public int slimeCount;
	public RPG() {
		team = new ArrayList<Role>(0);
		team.add(new NormalAttacker());
		x = y = 25;
		face = 2;
		isSlime = new boolean[5];
		for (int i = 0; i < isSlime.length; i++)
			isSlime[i] = true;
		slimeCount = 5;
	}
	public Role[] toArray() {
		Role[] tmp = new Role[0];
		return team.toArray(tmp);
	}
}
