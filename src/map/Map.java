package map;

import gui.*;
import texture.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;

import javax.imageio.*;
import javax.swing.*;

public abstract class Map extends JPanel implements KeyListener {
	private static final long serialVersionUID = -3423596461213288327L;
	
	private static final String sep = File.separator;
	private static final String path = "." + sep + "img" + sep + "map";
	private File file;
	private static final TexturePool pool = new TexturePool();
	protected static final int numWidth = Main.window_width / Texture.img_width;
	protected static final int numHeight = Main.window_height / Texture.img_height;
	
	public Map(TextureType[][] ttys, String filename) {
		this.setLayout(new FlowLayout());
		this.addKeyListener(this);
		
		System.err.println("read file " + filename + " from " + path);
		file = new File(path, filename);
		if (!file.exists()) {
			System.err.println("file " + (path + sep + filename) + " doesn\'t exist.");
			file.getParentFile().mkdirs();
			
			System.err.println("created image ... ");
			BufferedImage finalImg = new BufferedImage(Main.window_width, Main.window_height, BufferedImage.TYPE_INT_ARGB);
			for (int j = 0; j < numHeight; j++) {
				for (int i = 0; i < numWidth; i++) {
					finalImg.createGraphics().drawImage(pool.textureFactory(ttys[i][j]).getImage(), Texture.img_width * i, Texture.img_height * j, null);
				}
			}
			
			try {
				file.createNewFile();
				ImageIO.write(finalImg, "png", file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		ImageIcon icon = new ImageIcon(path + sep + filename);
		this.add(new JLabel(icon));
	}
	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
}
