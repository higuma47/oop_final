package texture;

import java.util.*;
import javax.swing.*;

public class TexturePool {
	public Texture[] texturePool;
	public TexturePool() {
		ArrayList<Texture> textureArray = new ArrayList<Texture>(0);
		for (TextureType tty : TextureType.values()) {
			textureArray.add(loadTexture(tty));
		}
		Texture[] tmp = new Texture[0];
		texturePool = textureArray.toArray(tmp);
	}
	public ImageIcon textureFactory(TextureType type) {
		for (Texture t : texturePool) {
			if (t.type == type)
				return t.tex;
		}
		return null;
	}
	private Texture loadTexture(TextureType type) {
		switch (type) {
		case Meadow:
			return new Meadow();
		case DirtWithMeadow:
			return new DirtWithMeadow();
		}
		return null;
	}
}
