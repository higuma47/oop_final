package texture;

public class Meadow extends Texture {
	public Meadow() {
		super(TextureType.Meadow, TextureClass.OutSideLand, true, "Outside_A2.png", 0, 0);
	}
}
