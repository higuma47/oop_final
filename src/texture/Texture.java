package texture;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

public abstract class Texture {
	public static final int img_width = 32;
	public static final int img_height = 32;
	protected enum TextureClass {
		OutSideLand;
	}

	private static final String sep = File.separator;
	private static final String path = "." + sep + "img" + sep + "texture";
	
	public ImageIcon tex;
	public TextureType type;
	public TextureClass typecls;
	public boolean walkable;
	public Texture(TextureType tty, TextureClass ttc, boolean bool, String filename, int x, int y) {
		type = tty;
		typecls = ttc;
		walkable = bool;
		switch (typecls) {
		case OutSideLand:
			Image scaledImg = getScaledImage(filename);
			ImageFilter ImgFt = new CropImageFilter((x + 1) * img_width, (y + 0) * img_height, img_width, img_height);
			Image Img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(scaledImg.getSource(), ImgFt));
			tex = new ImageIcon(Img);
			break ;
		}
	}
	private Image getScaledImage(String filename) {
		try {
			BufferedImage BufImg = ImageIO.read(new File(path + sep + filename));
			return BufImg.getScaledInstance(BufImg.getWidth(), BufImg.getHeight(), Image.SCALE_DEFAULT);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
