package texture;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

public class RoleTexture {
	public static final int img_width = 32;
	public static final int img_height = 32;
	private static final String sep = File.separator;
	private static final String path = "." + sep + "img";
	private static final String filename = "Actor1.png";
	public ImageIcon[] tex;
	public RoleTexture() {
		tex = new ImageIcon[4];
		Image scaledImg = getScaledImage();
		for (int i = 0; i < 4; i++) {
			ImageFilter ImgFt = new CropImageFilter(img_width, i * img_height, img_width, img_height);
			Image Img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(scaledImg.getSource(), ImgFt));
			tex[i] = new ImageIcon(Img);
		}
	}
	private Image getScaledImage() {
		try {
			BufferedImage BufImg = ImageIO.read(new File(path + sep + filename));
			return BufImg.getScaledInstance(BufImg.getWidth(), BufImg.getHeight(), Image.SCALE_DEFAULT);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
