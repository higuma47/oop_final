package chara;

import java.io.*;

public abstract class Role implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4821824905836180524L;
	public static int LOWEST_LEVEL = 1;
	public static int HIGHEST_LEVEL = 99;
	public static int MAX_ACC_ROUND = 3;
	
	public boolean alliance;
	
	public int level;
	public int accuExp;
	
	public int maxHP;
	public int maxMP;
	public int Atk;
	public int Def;
	public int MAtk;
	public int MDef;
	public int Spd;
	
	public int nowHP;
	public int nowMP;
	
	public String[] skillString;
	public String imagePath;
	public int imageWidth;
	public int imageHeigth;
	
	public int SkillNum;
	
	public int accuRound;
	
	public int expThreshold; 
	
	public void normalAttack(Role r) {
		// Normal Attack For every Role.
		
		// d : damage.
		int d;
		if(accuRound == 0) {
			d = Atk - r.Def;
		}
		else {
			// If there are accumulated rounds, normal attack will be more powerful. 
			d = Atk * accuRound - r.Def;
		}
		
		if(d <= 0) {
			d = 1;
		}
		
		int result = r.nowHP - d;
		if(result < 0) {
			result = 0;
		}
		r.nowHP = result;
	}
	
	public void waitRound() {
		// wait a round and increase the accumulated round by 1.
		int r = accuRound + 1;
		if(r > 3) {
			r = 3;
		}
	}
	
	public void increaseExp(int ex) {
		// When the character gets Exp, use this method to increase it.
		// And the method can also be used to determine level up or not.
		int afterIncrease = accuExp + ex;
		expThreshold = level * level * level;
		if(afterIncrease >= expThreshold) {
			// Level up !
			if(level < HIGHEST_LEVEL) {
				level++;
				expThreshold = level * level * level;
				accuExp = afterIncrease;
				adjustAbility(level);
			}
			else {
				// If level reaches the upper bound, then do nothing.
				accuExp = expThreshold;
			}
		}
		else {
			// Still not level up, just increases its accumulated Exp.
			accuExp = afterIncrease;
		}
	}
	abstract public void adjustAbility(int l);
}
