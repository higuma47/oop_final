package chara;

public class Swordman extends Role {
	public Swordman () {
		// For New Game : Create a new Character.
		level = 1;
		accuExp = 10;
		maxHP = 1000;
		maxMP = 200;
		Atk = 1000;
		Def = 5;
		MAtk = 0;
		MDef = 3;
		Spd = 15;
		nowHP = maxHP;
		nowMP = maxMP;
		SkillNum = 3;
		skillString = new String[3];
		skillString[0] = "Power Punch (-10 MP)";
		skillString[1] = "Rampage Kick (-10 MP, +2 Atk after this round)";
		skillString[2] = "Absorb Hit (-20MP, +(damage*0.1) HP from enemy)";
		imagePath = "./img/Swordman.png";
		imageWidth = 197;
		imageHeigth = 195;
	}
	public Swordman(int l, int exp, int mhp, int mmp, int atk, int def, int matk, int mdef, int spd, int nowhp, int nowmp) {
		// For Load Game : Get the existing value as its value.
		level = l;
		accuExp = exp;
		maxHP = mhp;
		maxMP = mmp;
		Atk = atk;
		Def = def;
		MAtk = matk;
		MDef = mdef;
		Spd = spd;
		nowHP = nowhp;
		nowMP = nowmp;
		SkillNum = 3;
		skillString = new String[3];
		skillString[0] = "Power Punch (-10 MP)";
		skillString[1] = "Rampage Kick (-10 MP, +2 Atk after this round)";
		skillString[2] = "Absorb Hit (-20MP, +(damage*0.1) HP from enemy)";
		imagePath = "./img/Swordman.png";
		imageWidth = 197;
		imageHeigth = 195;
	}
	
	public void adjustAbility(int l) {
		maxHP += 10;
		maxMP += 5;
		Atk += 10;
		Def += 5;
		MAtk += 2;
		MDef += 2;
		Spd += 5;
		
		nowHP += 10;
		nowMP += 5;
	}
	
	public boolean PowerPunch (Role r) {
		// One of its skill !
		// Need 10 MP to use it.
		if(nowMP < 10) {
			return false;
		}
		nowMP -= 10;
		
		// d : damage
		int d;
		d = 20 + Atk * accuRound - r.Def;
		
		if(d < 0) {
			d = 0;
		}
		int result = r.nowHP - d;
		if(result < 0) {
			result = 0;
		}
		r.nowHP = result;
		return true;
	}
	public boolean RampageKick(Role r) {
		if(nowMP < 10) {
			return false;
		}
		nowMP -= 10;
		
		// d : damage
		int d;
		d = 12 + Atk * accuRound - r.Def;
		
		if(d < 0) {
			d = 0;
		}
		int result = r.nowHP - d;
		if(result < 0) {
			result = 0;
		}
		r.nowHP = result;
		
		Atk += 2;
		
		return true;
	}
	public boolean AbsorbHit(Role r) {
		if(nowMP < 20) {
			return false;
		}
		nowMP -= 20;
		
		// d : damage
		int d;
		d = 30 + Atk * accuRound - r.Def;
		
		if(d < 0) {
			d = 0;
		}
		int result = r.nowHP - d;
		if(result < 0) {
			result = 0;
		}
		r.nowHP = result;
		
		int me = nowHP + (result / 10);
		if(me > maxHP) {
			me = maxHP;
		}
		
		nowHP = me;
		
		return true;
	}
}
