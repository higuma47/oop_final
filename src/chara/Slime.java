package chara;

public class Slime extends Role {
	public Slime() {
		level = 1;
		accuExp = 1;
		maxHP = 20;
		maxMP = 5;
		Atk = 2;
		Def = 2;
		MAtk = 0;
		MDef = 2;
		Spd = 20;
		nowHP = maxHP;
		nowMP = maxMP;
		skillString = new String[3];
		skillString[0] = "";
		skillString[1] = "";
		skillString[2] = "";
		imagePath = "./img/Slime.png";
		imageWidth = 136;
		imageHeigth = 47;
		SkillNum = 0;
	}
	public void adjustAbility(int l) {}
}
